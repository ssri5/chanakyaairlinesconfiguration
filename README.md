# Replicating Chanakya Airlines Chatbot Project #

1. The Backend code is available in [another public repository][backend]. The code is created in Java, using the Eclipse IDE. The code shall be deployed as an AWS Lambda function. Follow the [tutorial at AWS][aws-eclipse] to know how to use the code in the repository.
1. You will have to put your AWS credentials, region and AWS S3 bucket name in the [src/main/resources/aws.properties][properties-file] file in the above repository.
1. We used Dialogflow for building the chatbots (there are two versions). The Chatbots can be imported in Dialogflow using the _Chanakya-Airlines-Bot-v**X**.zip_ files.
1. A comparative Webpage that hosts the versions are zipped under the file _chanakya.zip_, provided as part of this repository.
1. In Dialogflow's Integrations section, enable the **Dialogflow Messenger** option. Note down the agent id of the two versions. They should be configured in the _v1.html_ and _v2.html_ files present in the _chanakya.zip_ file.
1. We used [AWS API Gateway][api-gateway] to create a REST API Endpoint to access the background operations. The API must enable a **POST** endpoint. In order to map the inputs received from Dialogflow to the input json that the background operations code accepts, as well as map the response received from the operations to get processed by Dialogflow, two [mapping templates][mapping] are required. They are provided in the _Integration Request Mapping Template.vm_ and _Integration Response Mapping Template.vm_ files.

The deployment plan for the chatbots are shown below:

![https://bitbucket.org/ssri5/chanakyaairlinesconfiguration/raw/049db73fc521f4af0cf358b03f80a18b62b82b1d/AirlineOperations.png](https://bitbucket.org/ssri5/chanakyaairlinesconfiguration/raw/049db73fc521f4af0cf358b03f80a18b62b82b1d/AirlineOperations.png)

[backend]: https://bitbucket.org/ssri5/airlineoperationsbackend
[aws-eclipse]: https://docs.aws.amazon.com/toolkit-for-eclipse/v1/user-guide/lambda-tutorial.html
[properties-file]: https://bitbucket.org/ssri5/airlineoperationsbackend/src/master/src/main/resources/aws.properties
[mapping]: https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-mapping-template-reference.html
[api-gateway]: https://docs.aws.amazon.com/apigateway/latest/developerguide/getting-started.html